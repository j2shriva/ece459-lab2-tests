OUTPUT=$(cargo run --release --bin message-passing -- $1 $2 $3);
if [[ "$OUTPUT" == "$4" ]]
then
    echo Pass
else
    echo ""
    echo "    TEST CASE FAILED"
    echo "    Expected [$4], got [$OUTPUT] for JWT [$1]"
    echo ""
    exit 1
fi