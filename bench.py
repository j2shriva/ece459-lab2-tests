from asyncio import subprocess
import time

import subprocess
def main():
    res = []
    with open("bench.txt") as f:
        for line in f.readlines():
            start = time.time()
            run = subprocess.run("bash mp.sh " + line, shell=True)
            try:
                run.check_returncode()
            except:
                print("Failure")
                return
            end = time.time()
            mp_time = end-start

            start = time.time()
            run = subprocess.run("bash sm.sh " + line, shell=True)
            try:
                run.check_returncode()
            except:
                print("Failure")
                return
            end = time.time()
            sm_time = end-start

            start = time.time()
            run = subprocess.run("bash l2.sh " + line, shell=True)
            try:
                run.check_returncode()
            except:
                print("Failure")
                return
            end = time.time()
            l2_time = end-start

            tmp = line.split(" ")
            if len(tmp) > 4:
                tmp = tmp[:-3]
                tmp.append("")
            jwt, max_len, alph, ans = tmp

            print(ans)
            
            
            res.append([l2_time, mp_time, sm_time, len(alph.strip()), len(ans.strip())])

    for l2_time, mp_time, sm_time, len_alph, len_ans in res:
        print(len_alph, len_ans)
        print(l2_time, mp_time, sm_time)



if __name__ == "__main__":
    main()